package com.example.favmovies.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.favmovies.R
import androidx.fragment.app.setFragmentResult
import com.example.favmovies.MovieDataClass
import com.example.favmovies.databinding.FragmentMovieBinding
import com.example.favmovies.extensions.setImage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class MovieFragment : BaseFragment<FragmentMovieBinding>(FragmentMovieBinding::inflate) {

    private val args: MovieFragmentArgs by navArgs()

    private val movieResult get() = args.movieResult

    private lateinit var auth:FirebaseAuth
    private lateinit var databaseReference: DatabaseReference




    override fun start() {

        binding.addToFavorites.setOnClickListener {
            addToFavorites()
        }




        Log.d("movieResult", "start: $movieResult.")
        binding.cover.setImage("${movieResult.posterPath}")
        binding.title.text = "Title : ".plus(movieResult.title)
        binding.imdb.text = "Rating : " + movieResult.voteAverage
        binding.releaseDate.text = "Release date : " + movieResult.releaseDate
        binding.overview.text = "Overview : " + movieResult.overview


        binding.cover.setOnClickListener {
            setFragmentResult("requestKey", bundleOf("bundleKey" to movieResult.posterPath))
            findNavController().navigate(R.id.action_movieFragment_to_coverFragment)
        }


    }


    private fun addToFavorites() {
        binding.progressbar.visibility = View.VISIBLE
        auth = FirebaseAuth.getInstance()
        val uid = auth.currentUser?.uid
        databaseReference = FirebaseDatabase.getInstance().getReference("Users")

        val cover = movieResult.posterPath
        val title = movieResult.title
        val imdb = movieResult.voteAverage
        val releaseDate = movieResult.releaseDate
        val overview = movieResult.overview

        val movie = MovieDataClass(cover, title, imdb.toString(), releaseDate, overview)

        if (uid!= null){

            databaseReference.child(uid).setValue(movie).addOnCompleteListener{
                Toast.makeText(requireContext(), "Added to favorites", Toast.LENGTH_SHORT).show()
                binding.progressbar.visibility = View.GONE

            }

        }


    }

}