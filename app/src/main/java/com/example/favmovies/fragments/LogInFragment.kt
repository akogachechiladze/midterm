package com.example.favmovies.fragments


import android.content.Context
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import androidx.navigation.fragment.findNavController
import com.example.favmovies.R
import com.example.favmovies.databinding.FragmentLogInBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

val Context.dataStore: DataStore<Preferences> by preferencesDataStore("Info")

class LogInFragment : BaseFragment<FragmentLogInBinding>(FragmentLogInBinding::inflate) {
    private lateinit var auth: FirebaseAuth


    override fun start() {
        auth = Firebase.auth


        binding.logInButton.setOnClickListener{
            signIn()
        }

        binding.registerButton.setOnClickListener {
            findNavController().navigate(R.id.action_logInFragment_to_registerFragment)
        }


    }



    private fun signIn() {
        val email = binding.emailET.text.toString()
        val password = binding.password.text.toString()

        if (email != "" && password != "" && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.progressbar.visibility = View.VISIBLE
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("Succesfull", "signInWithEmail:success")
                        val user = auth.currentUser
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("Failure", "signInWithEmail:failure", task.exception)
                        Toast.makeText(requireContext(), "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }

            binding.progressbar.visibility = View.GONE

            findNavController().navigate(R.id.action_logInFragment_to_homeFragment)





        }else if (email != "" && password == "" && Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(requireContext(), "Enter your password", Toast.LENGTH_SHORT).show()
        }else if (email == "" && password != "" && Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(requireContext(), "Enter your email", Toast.LENGTH_SHORT).show()
        }else {
            Toast.makeText(requireContext(), "Invalid email", Toast.LENGTH_SHORT).show()
        }


    }





}