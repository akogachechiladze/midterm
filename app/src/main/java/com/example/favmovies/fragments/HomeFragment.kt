package com.example.favmovies.fragments

import android.R
import android.content.res.Configuration
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.favmovies.FavMoviesData
import com.example.favmovies.Movies
import com.example.favmovies.adapter.RecyclerViewAdapter1
//import com.example.favmovies.adapter.RecyclerViewAdapterFavorites
import com.example.favmovies.databinding.FragmentHomeBinding
import com.example.favmovies.network.NetworkConnection
import com.google.firebase.database.DatabaseReference
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext



class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate), AdapterView.OnItemSelectedListener {



    var options = arrayOf("Top Rated", "Popular", "Favorites")

    var spinner: Spinner? = null

    var selectedItem = "Top Rated"


    private val viewModel: ViewModel by viewModels()
    private lateinit var adapter: RecyclerViewAdapter1





    override fun start() {
        checkConnection()
        binding.swipeRefresh.setOnRefreshListener {
            position()
        }
        initSpinner()
        binding.swipeRefresh.isRefreshing = true
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {

            initRecyclerView(3, 25F)

        } else {

            initRecyclerView(2, 45F)


        }
        binding.swipeRefresh.isRefreshing = false
    }

    private fun initRecyclerView(spanCount: Int, textSize: Float) {


        if (selectedItem == "Top Rated") {
            binding.appNameTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
            binding.recyclerView.layoutManager = GridLayoutManager(requireContext(), spanCount)
            adapter = RecyclerViewAdapter1(::onMovieClick)
            binding.recyclerView.adapter = adapter

            lifecycleScope.launchWhenCreated {
                Log.d("progressbar", "start:${binding.swipeRefresh.isRefreshing} ")

                viewModel.loadTopRatedMovies().observe(viewLifecycleOwner, {
                    adapter.submitData(lifecycle, it)
                })

            }
            binding.swipeRefresh.isRefreshing = false

        }else if (selectedItem == "Popular") {
            binding.appNameTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
            binding.recyclerView.layoutManager = GridLayoutManager(requireContext(), spanCount)
            adapter = RecyclerViewAdapter1(::onMovieClick)
            binding.recyclerView.adapter = adapter

            binding.swipeRefresh.isRefreshing = true
            lifecycleScope.launchWhenCreated {
                viewModel.loadPopularMovies().observe(viewLifecycleOwner, {
                    adapter.submitData(lifecycle, it)
                })
            }
        }
            binding.swipeRefresh.isRefreshing = false
//        }else if (selectedItem == "Favorites"){
//
//            val items = mutableListOf<FavMoviesData.Users.FavMovieInfo>()
//
//            binding.appNameTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize)
//            binding.recyclerView.layoutManager = GridLayoutManager(requireContext(), spanCount)
//            adapter = RecyclerViewAdapterFavorites(items)
//            binding.recyclerView.adapter = adapter
//
//            lifecycleScope.launchWhenCreated {
//                viewModel.loadPopularMovies().observe(viewLifecycleOwner, {
//                    adapter.submitData(lifecycle, it)
//                })
//            }
//
//        }

    }

    private fun position() {
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {

            initRecyclerView(3, 25F)

        } else {

            initRecyclerView(2, 45F)

        }
    }

    private fun checkConnection() {
        //check network conn
        val networkConnection = NetworkConnection(requireContext())
        networkConnection.observe(this, Observer { isConnected ->
            if (isConnected) {
                binding.lostConnection.visibility = View.GONE
                binding.progressbar.visibility = View.INVISIBLE
            }else {
                binding.lostConnection.visibility = View.VISIBLE
                binding.progressbar.visibility = View.VISIBLE
            }
        })
    }


//    private suspend fun write() {
//        requireContext().dataStore.edit {
//            it[searchTextkey] = searchText
//        }
//
//    }


    private fun initSpinner() {
        spinner = binding.spinner
        spinner!!.onItemSelectedListener = this

        val aa = ArrayAdapter(requireContext(), R.layout.simple_spinner_item, options)
        aa.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner!!.adapter = aa
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        binding.optionsTV.text = options[p2]
        selectedItem = options[p2]
        Log.d("selectedItem", "onItemSelected: $selectedItem")
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }



    private fun onMovieClick(movie: Movies.Result) {
        val action = HomeFragmentDirections.actionHomeFragmentToMovieFragment(movie)
        findNavController().navigate(action)
    }


}