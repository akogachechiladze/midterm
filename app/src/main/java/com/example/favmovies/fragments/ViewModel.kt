package com.example.favmovies.fragments


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.example.favmovies.pagingSouce.PagingSourcePopular
import com.example.favmovies.pagingSouce.PagingSourceTopRated
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class ViewModel: ViewModel() {
    private lateinit var reference: DatabaseReference


    fun loadTopRatedMovies() =
            Pager(config = PagingConfig(1), pagingSourceFactory = { PagingSourceTopRated() })
                    .liveData.cachedIn(viewModelScope)


    fun loadPopularMovies() =
            Pager(config = PagingConfig(1), pagingSourceFactory = { PagingSourcePopular() })
                    .liveData.cachedIn(viewModelScope)

    fun readData() {
        reference = FirebaseDatabase.getInstance().getReference("Users")
    }
}
