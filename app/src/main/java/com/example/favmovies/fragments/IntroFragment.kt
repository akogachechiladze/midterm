package com.example.favmovies.fragments


import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.favmovies.R
import com.example.favmovies.databinding.FragmentIntroBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class IntroFragment : BaseFragment<FragmentIntroBinding>(FragmentIntroBinding::inflate) {



    override fun start() {

        val anim1 = AnimationUtils.loadAnimation(context, R.anim.animationup)
        val anim2 = AnimationUtils.loadAnimation(context, R.anim.animationdown)
        val anim3 = AnimationUtils.loadAnimation(context, R.anim.animationright)
        anim1.duration = 3500
        anim2.duration = 3500
        anim3.duration = 3500
        binding.logo.startAnimation(anim1)
        binding.appTitle.startAnimation(anim2)
        binding.cola.startAnimation(anim3)

        Handler(Looper.getMainLooper()).postDelayed({
                                                    findNavController().navigate(R.id.action_introFragment_to_logInFragment)
//            viewLifecycleOwner.lifecycleScope.launch {
//                withContext(Dispatchers.IO) {
//                    read()
//                }
//            }
        }, 3500)


    }


//    private suspend fun read() {
//        val email = requireContext().dataStore.data.map { preference ->
//            preference[RegisterFragment().emailkey] ?: ""
//        }.first()
//
//        val password = requireContext().dataStore.data.map { preference ->
//            preference[RegisterFragment().passwordkey] ?: ""
//        }.first().toString()
//
//    }


}