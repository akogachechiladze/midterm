package com.example.favmovies.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.favmovies.R
import com.example.favmovies.databinding.FragmentCoverBinding
import com.example.favmovies.extensions.setImage


class CoverFragment : BaseFragment<FragmentCoverBinding>(FragmentCoverBinding::inflate) {
    override fun start() {
        init()
        binding.movieBackButton.setOnClickListener {
            findNavController().navigate(R.id.action_coverFragment_to_movieFragment)
        }
    }

    private fun init() {
        setFragmentResultListener("requestKey") { requestKey, bundle ->
            val coverUrl = bundle.getString("bundleKey")

            binding.coverPhoto.setImage(coverUrl)
        }



    }

}