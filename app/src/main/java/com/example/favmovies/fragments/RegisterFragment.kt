package com.example.favmovies.fragments


import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.favmovies.R
import com.example.favmovies.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegisterFragment : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {

    private lateinit var auth: FirebaseAuth

    val emailkey = stringPreferencesKey("email")
    val passwordkey = stringPreferencesKey("password")

    override fun start() {
        auth = Firebase.auth
        binding.signUpButton.setOnClickListener {
            signUp()
        }
    }

    private fun signUp() {
        binding.progressbar.visibility = View.VISIBLE
        val email = binding.emailET.text.toString()
        val password = binding.password.text.toString()
        val repeatPassword = binding.repeatPassword.text.toString()

        if (email != "" && password != "" && password == repeatPassword && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {

            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("successful", "createUserWithEmail:success")
                        val user = auth.currentUser
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("failure", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(requireContext(), "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }

            binding.progressbar.visibility = View.GONE

            viewLifecycleOwner.lifecycleScope.launch {
                withContext(Dispatchers.IO){
                    write(email, password)
                }
            }

            findNavController().navigate(R.id.action_registerFragment_to_logInFragment)





        }else if (email != "" && (password == "" || repeatPassword == "") && Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(requireContext(), "Enter your password", Toast.LENGTH_SHORT).show()
        }else if (email == "" && password != "" &&repeatPassword != ""&& Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(requireContext(), "Enter your email", Toast.LENGTH_SHORT).show()
        }else if (email != "" && password != "" && repeatPassword != ""  && password != repeatPassword&& Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(requireContext(), "Check password fields", Toast.LENGTH_SHORT).show()
        }else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(requireContext(), "Invalid email", Toast.LENGTH_SHORT).show()
        }


    }

    private suspend fun write(email: String, password: String) {

        requireContext().dataStore.edit {
            it[emailkey] = email
            it[passwordkey] = password
        }

    }
}