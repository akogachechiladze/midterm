package com.example.favmovies


import com.google.gson.annotations.SerializedName

data class FavMoviesData(
    @SerializedName("Users")
    val users: Users?
){data class Users(
    @SerializedName("xyqAMAQRdHUTCFTKo633Pcdq2Z53")
    val userId: FavMovieInfo?
){data class FavMovieInfo(
    @SerializedName("cover")
    val cover: String?,
    @SerializedName("imdb")
    val imdb: String?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("releaseDate")
    val releaseDate: String?,
    @SerializedName("title")
    val title: String?
)}}