package com.example.favmovies.network

import com.example.favmovies.Movies
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("movie/top_rated?api_key=9da7a6b87622a920b5ea23f159d58f15")
    suspend fun getTopRatedMovies(@Query("page")page: Int): Response<Movies>


    @GET("movie/popular?api_key=9da7a6b87622a920b5ea23f159d58f15")
    suspend fun getPopularMovies(@Query("page")page: Int): Response<Movies>




}