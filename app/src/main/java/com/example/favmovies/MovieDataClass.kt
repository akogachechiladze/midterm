package com.example.favmovies

data class MovieDataClass (val cover: String? = null, val title:String? = null, val imdb:String? = null, val releaseDate:String? = null, val overview:String? = null)