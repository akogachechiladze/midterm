package com.example.favmovies.pagingSouce

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.favmovies.Movies
import com.example.favmovies.network.NetworkClient

class PagingSourceTopRated : PagingSource<Int, Movies.Result>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movies.Result> {
        val page: Int = params.key ?: 1


        return try {
            val response = NetworkClient.apiClientTopRated.getTopRatedMovies(page)
            val body = response.body()

            if (response.isSuccessful && body != null) {
                var prevPage: Int? = null
                var nextPage: Int? = null
                if (body.totalPages!! > body.page!!) {
                    nextPage = page + 1
                }

                if (page != 1) {
                    prevPage = page - 1
                }
                LoadResult.Page(
                        body.results!!,
                        prevPage,
                        nextPage
                )
            } else {
                LoadResult.Error(Throwable())
            }
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Movies.Result>): Int? {
        TODO("Not yet implemented")
    }


}