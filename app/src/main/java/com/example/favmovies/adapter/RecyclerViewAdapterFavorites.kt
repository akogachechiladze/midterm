//package com.example.favmovies.adapter
//
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import androidx.paging.PagingDataAdapter
//import androidx.recyclerview.widget.DiffUtil
//import androidx.recyclerview.widget.RecyclerView
//import com.example.favmovies.FavMoviesData
//import com.example.favmovies.Movies
//import com.example.favmovies.databinding.ItemsLayoutBinding
//import com.example.favmovies.extensions.setImage
//
//class RecyclerViewAdapterFavorites(var items: MutableList<FavMoviesData.Users.FavMovieInfo>) :
//    RecyclerView.Adapter<RecyclerViewAdapterFavorites.ViewHolder>() {
//
//
//    inner class ViewHolder(val binding: ItemsLayoutBinding) :
//        RecyclerView.ViewHolder(binding.root) {
//
//
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        ItemsLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.binding.homefragmentIV.setImage(items[position].cover)
//        holder.binding.titleTV.setText(items[position].title)
//    }
//
//    override fun getItemCount()= items.size
//
//
//    fun setData(data: MutableList<FavMoviesData.Users.FavMovieInfo>) {
//        this.items = data
//        notifyDataSetChanged()
//    }
//}
