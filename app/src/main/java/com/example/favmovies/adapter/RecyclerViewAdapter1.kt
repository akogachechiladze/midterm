package com.example.favmovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.favmovies.Movies
import com.example.favmovies.databinding.ItemsLayoutBinding
import com.example.favmovies.extensions.setImage


class RecyclerViewAdapter1(private val onMovieClick: (movie: Movies.Result) -> Unit) : PagingDataAdapter<Movies.Result, RecyclerViewAdapter1.ViewHolder>(
        CallBack()
) {

    inner class ViewHolder(val binding: ItemsLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(model: Movies.Result) {
            binding.titleTV.text = model.title
            binding.homefragmentIV.setImage(model.posterPath)
            itemView.setOnClickListener {
                onMovieClick(model)
            }



        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
            ItemsLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(getItem(position)!!)

//        holder.itemView.setOnClickListener { getItem(position)?.let { it1 -> onMovieClick(it1) } }
    }

    class CallBack : DiffUtil.ItemCallback<Movies.Result>() {
        override fun areItemsTheSame(oldItem: Movies.Result, newItem: Movies.Result): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Movies.Result, newItem: Movies.Result): Boolean {
            return oldItem == newItem
        }

    }







}