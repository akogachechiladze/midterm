package com.example.favmovies.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.favmovies.R



fun ImageView.setImage(url: String?) {
    if(!url.isNullOrEmpty()) {
        Glide.with(this).load("https://image.tmdb.org/t/p/w500/$url").placeholder(R.mipmap.blackscreen).into(this)
    }
    else{
        setImageResource(R.mipmap.blackscreen)
    }
}